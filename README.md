# flutter_thirdlogin_plugin



## 功能说明
* 第三方登录授权逻辑封装，包括：QQ、微信  

## 注意事项
* QQ登录APPID需要按照下面方式在工程中设置：
```  
android {  
    ...
    defaultConfig{
        ...
        manifestPlaceholders = [QQ_APP_ID: "填写你的qq appId"]
        ...
    }
    ...
}
```

## 三、使用方式
```
dependencies:
    flutter_thirdlogin_plugin:
        git:
            url: https://gitee.com/rishli/flutter_thirdlogin_plugin.git
            ref: (tag标签)
```