package com.qw.flutter.thirdlogin.util;

import android.util.Log;

import com.qw.flutter.thirdlogin.BuildConfig;


/**
 * 功能描述:二次封装日志打印工具类
 */
public class LogUtils {
    /**
     * 仅开发模式下打印日志
     */
    private static boolean IS_DEBUG = BuildConfig.LOGIN_DEBUG;
    private final static String LOG_TAG = "thirdLogin>>> ";

    public static void i(String msg) {
        if (IS_DEBUG) {
            Log.i(LOG_TAG, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (IS_DEBUG) {
            Log.i(LOG_TAG + tag, msg);
        }
    }

    public static void v(String msg) {
        if (IS_DEBUG) {
            Log.v(LOG_TAG, msg);
        }
    }

    public static void v(String tag, String msg) {
        if (IS_DEBUG) {
            Log.i(LOG_TAG + tag, msg);
        }
    }

    public static void d(String msg) {
        if (IS_DEBUG) {
            Log.d(LOG_TAG,msg);
        }
    }

    public static void d(String tag, String msg) {
        if (IS_DEBUG) {
            Log.d(LOG_TAG + tag, msg);
        }
    }

    public static void w(String msg) {
        if (IS_DEBUG) {
            Log.w(LOG_TAG, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (IS_DEBUG) {
            Log.w(LOG_TAG + tag, msg);
        }
    }

    public static void e(String msg) {
        if (IS_DEBUG) {
            Log.e(LOG_TAG, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (IS_DEBUG) {
            Log.e(LOG_TAG + tag, msg);
        }
    }

}
