package com.qw.flutter.thirdlogin.qq;

/**
 * @date: 2021/5/14 13:51
 * @author: lixu
 * @description: QQ授权后获取的信息
 */
public class QQAuthBean {
    /**
     * qq授权信息
     */
    private String openId;
    private String accessToken;
    /**
     * 授权错误提示
     */
    private String msg;

    public QQAuthBean() {
    }

    public QQAuthBean(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
