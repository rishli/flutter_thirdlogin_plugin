package com.qw.flutter.thirdlogin;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.qw.flutter.thirdlogin.qq.QQLoginWrapper;
import com.qw.flutter.thirdlogin.util.LogUtils;
import com.qw.flutter.thirdlogin.wxapi.WechatWrapper;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * FlutterThirdLoginPlugin
 * 方法回调顺序
 * 1、onAttachedToEngine
 * 2、onAttachedToActivity
 * 4、onDetachedFromActivity
 * 5、onDetachedFromEngine
 */
public class FlutterThirdLoginPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
    private static final String TAG = "FlutterThirdLoginPlugin";
    private MethodChannel channel;
    private Context context;
    private Activity activity;
    private ActivityPluginBinding binding;

    /**
     * 微信登录授权
     */
    private static final String WECHAT_AUTH_METHOD = "wechatAuthMethod";
    private static final String QQ_AUTH_METHOD = "qqAuthMethod";
    private static final String APPID_FIELD = "appId";
    private static final String SCOPE_FIELD = "scope";
    private static final String STATE_FIELD = "state";

    /**
     * 微信APPID
     */
    public static String WECHAT_APPID;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        context = flutterPluginBinding.getApplicationContext();
        channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "flutter_thirdlogin_plugin");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        String method = call.method;
        LogUtils.d(TAG, "onMethodCall:" + method);
        if (WECHAT_AUTH_METHOD.equals(method)) {
            //调用微信授权获取code返回
            String appId = call.argument(APPID_FIELD);
            WECHAT_APPID = appId;

            String scope = call.argument(SCOPE_FIELD);
            String state = call.argument(STATE_FIELD);

            WechatWrapper.create(context, appId).startWechatLoginAuth(context, scope, state, result);
        } else if (QQ_AUTH_METHOD.equals(method)) {
            //调用QQ授权获取info返回到flutter
            String appId = call.argument(APPID_FIELD);
            String scope = call.argument(SCOPE_FIELD);

            QQLoginWrapper.create().startQQLoginAuth(activity, binding, appId, scope, result);
        } else {
            result.notImplemented();
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        LogUtils.i(TAG, "onDetachedFromEngine");
        channel.setMethodCallHandler(null);
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        LogUtils.i(TAG, "onAttachedToActivity:" + binding.getActivity());
        this.binding = binding;
        this.activity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        LogUtils.i(TAG, "onDetachedFromActivityForConfigChanges");
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
        LogUtils.i(TAG, "onReattachedToActivityForConfigChanges");
    }

    @Override
    public void onDetachedFromActivity() {
        LogUtils.i(TAG, "onDetachedFromActivity");
    }

}
