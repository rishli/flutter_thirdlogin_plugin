package com.qw.flutter.thirdlogin.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.qw.flutter.thirdlogin.FlutterThirdLoginPlugin;
import com.qw.flutter.thirdlogin.util.LogUtils;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;


/**
 * 微信授权回调
 */
public class FlutterWXEntryActivity extends Activity implements IWXAPIEventHandler {
    private static final String TAG = "WXEntryActivity";
    private IWXAPI api;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.d(TAG, "onCreate");

        api = WXAPIFactory.createWXAPI(this, FlutterThirdLoginPlugin.WECHAT_APPID, true);
        try {
            Intent intent = getIntent();
            api.handleIntent(intent, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        LogUtils.d(TAG, "onNewIntent");
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq req) {
        LogUtils.d(TAG, "onReq:" + req.getType());
        finish();
    }

    @Override
    public void onResp(BaseResp resp) {
        LogUtils.d(TAG, "onResp type:" + resp.getType());
        LogUtils.d(TAG, "onResp errCode:" + resp.errCode);

        //微信登录授权回调
        if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
            SendAuth.Resp authResp = (SendAuth.Resp) resp;
            String code = authResp.code;

            LogUtils.d(TAG, "onResp code:" + code);
            LogUtils.d(TAG, "onResp state:" + authResp.state);
            LogUtils.d(TAG, "onResp authResult:" + authResp.authResult);
            LogUtils.d(TAG, "onResp url:" + authResp.url);
            LogUtils.d(TAG, "onResp openId:" + authResp.openId);

            //登录授权回调
            if (resp.errCode == BaseResp.ErrCode.ERR_OK) {
                if (TextUtils.isEmpty(code)) {
                    //授权失败
                    WechatWrapper.setAuthResult(false, code, "授权失败");
                } else {
                    //授权成功
                    WechatWrapper.setAuthResult(true, code, "success");
                }
            } else if (resp.errCode == BaseResp.ErrCode.ERR_USER_CANCEL) {
                //授权取消
                WechatWrapper.setAuthResult(false, code, "取消授权");
            } else if (resp.errCode == BaseResp.ErrCode.ERR_AUTH_DENIED) {
                //授权取消
                WechatWrapper.setAuthResult(false, code, "拒绝授权");
            } else {
                //授权失败
                WechatWrapper.setAuthResult(false, code, "授权失败");
            }
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}