import 'dart:async';

import 'package:flutter/services.dart';

import 'bean/qq_auth_bean.dart';
import 'bean/wechat_auth_bean.dart';

///三方登录插件
///微信登录，
///参考：https://developers.weixin.qq.com/doc/oplatform/Mobile_App/WeChat_Login/Development_Guide.html
///QQ参考：https://wiki.open.qq.com/index.php?title=Android_SDK%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA
class ThirdLoginPlugin {
  static const MethodChannel _channel = const MethodChannel('flutter_thirdlogin_plugin');

  ///获取微信登录授权信息
  static const String _wechatAuthMethod = "wechatAuthMethod";
  static const String _qqAuthMethod = "qqAuthMethod";

  ///微信登录获取授权信息
  ///[wechatAppId] 微信登录appId
  ///[scope]应用授权作用域，如获取用户个人信息则填写 snsapi_userinfo
  ///[state]用于保持请求和回调的状态，授权请求后原样带回给第三方。该参数可用于防止 csrf 攻击（跨站请求伪造攻击），
  ///       建议第三方带上该参数，可设置为简单的随机数加 session 进行校验
  static Future<WechatAuthBean> getWechatLoginAuth(String wechatAppId, {String scope = 'snsapi_userinfo', String? state}) async {
    var data = await _channel.invokeMethod(_wechatAuthMethod, {
      'appId': wechatAppId,
      'scope': scope,
      'state': state ?? 'wechat_login',
    });
    return WechatAuthBean.fromJsonMap(data);
  }

  ///QQ登录获取授权信息
  ///[qqAppId] qq登录appId
  ///[scope]应用需要获得哪些接口的权限，由“,”分隔，例如：SCOPE = “get_user_info,add_topic”；如果需要所有权限则使用"all"。
  static Future<QQAuthBean> getQQLoginAuth(String qqAppId, {String scope = 'all'}) async {
    var data = await _channel.invokeMethod(_qqAuthMethod, {
      'appId': qqAppId,
      'scope': scope,
    });
    return QQAuthBean.fromJsonMap(data);
  }
}
