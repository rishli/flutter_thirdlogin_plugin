///@date:  2021/5/14 10:41
///@author:  lixu
///@description:微信授权响应对象
class WechatAuthBean {
  String? msg;

  ///微信sdk返回的code，提交到后端，授权失败为null
  String? code;

  ///是否授权成功
  bool isSuccess = false;

  WechatAuthBean.fromJsonMap(Map<dynamic, dynamic> map) {
    msg = map['msg'];
    code = map['code'];
    isSuccess = map['isSuccess'];
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<String, dynamic>();
    data['msg'] = msg;
    data['code'] = code;
    data['isSuccess'] = isSuccess;
    return data;
  }

  ///校验授权是否成功
  bool check() {
    return isSuccess && code != null && code!.isNotEmpty;
  }

  @override
  String toString() {
    return 'WechatAuthBean{msg: $msg, code: $code, isSuccess: $isSuccess}';
  }
}
