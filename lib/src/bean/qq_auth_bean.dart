///@date:  2021/5/14 15:41
///@author:  lixu
///@description:QQ授权响应对象
class QQAuthBean {
  String? msg;

  ///qq授权信息
  String? openId;
  String? accessToken;

  ///是否授权成功
  bool isSuccess = false;

  QQAuthBean.fromJsonMap(Map<dynamic, dynamic> map) {
    msg = map['msg'];
    openId = map['openId'];
    accessToken = map['accessToken'];
    isSuccess = map['isSuccess'];
  }

  Map<dynamic, dynamic> toJson() {
    final Map<dynamic, dynamic> data = new Map<String, dynamic>();
    data['msg'] = msg;
    data['openId'] = openId;
    data['accessToken'] = accessToken;
    data['isSuccess'] = isSuccess;
    return data;
  }

  ///校验授权是否成功
  bool check() {
    return isSuccess && openId != null && openId!.isNotEmpty && accessToken != null && accessToken!.isNotEmpty;
  }

  @override
  String toString() {
    return 'QQAuthBean{msg: $msg, openId: $openId, accessToken: $accessToken, isSuccess: $isSuccess}';
  }
}
