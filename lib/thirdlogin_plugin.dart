///@date:  2021/5/14 10:41
///@author:  lixu
///@description:
library flutter_thirdlogin_plugin;

export 'src/flutter_thirdlogin_plugin.dart';
export 'src/bean/wechat_auth_bean.dart';
export 'src/bean/qq_auth_bean.dart';
