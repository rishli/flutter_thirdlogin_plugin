import 'package:flutter/material.dart';
import 'package:flutter_nativedialog_plugin/loadingdialog_plugin.dart';
import 'package:flutter_thirdlogin_plugin/thirdlogin_plugin.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ///QQ
  String qqAppId = "101842286";

  ///微信开放平台APPID
  String wechatAppId = "wx8f62b95d99b0c206";

  String _wechatLoginInfo = "";
  String _qqLoginInfo = "";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('ThirdLogin example app'),
        ),
        body: Column(
          children: [
            Text('微信授权信息：$_wechatLoginInfo'),
            getButton('微信登录获取授权信息', () async {
              LoadingDialogPlugin.showLoading(msg: '微信登录授权', canCancel: true);
              WechatAuthBean bean = await ThirdLoginPlugin.getWechatLoginAuth(wechatAppId);
              print("微信授权完成：" + bean.toString());
              LoadingDialogPlugin.hideLoading();
              _wechatLoginInfo = bean.toString();
              setState(() {});
            }),
            Text('QQ授权信息：$_qqLoginInfo'),
            getButton('QQ登录获取授权信息', () async {
              LoadingDialogPlugin.showLoading(msg: 'QQ登录授权', canCancel: true);
              QQAuthBean bean = await ThirdLoginPlugin.getQQLoginAuth(qqAppId);
              print("QQ授权完成：" + bean.toString());
              LoadingDialogPlugin.hideLoading();
              _qqLoginInfo = bean.toString();
              setState(() {});
            }),
          ],
        ),
      ),
    );
  }

  ///获取通用演示按钮
  static Widget getButton(String text, VoidCallback onPressed) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: RaisedButton(
        textColor: Colors.white,
        color: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12),
          child: Text(
            text,
            textAlign: TextAlign.center,
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
